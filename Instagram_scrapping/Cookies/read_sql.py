import sqlite3
import pandas as pd
from Crypto.Cipher import AES
import os
import json
import base64
import win32crypt


path = r"C:\Users\param\AppData\Local\Google\Chrome\User Data\Default\Cookies"
cnx = sqlite3.connect(path)
cnx.text_factory = bytes
def select_all_tasks(conn):
    """
    Query all rows in the tasks table
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    cur.execute("SELECT * FROM cookies")

    rows = cur.fetchall()

    for row in rows:
        print(row)
# select_all_tasks(cnx)
path = r'%LocalAppData%\Google\Chrome\User Data\Local State'
path = os.path.expandvars(path)
# print(f"Path: {path}")
with open(path, 'r', encoding='ascii') as file:
    print("**********Opening file************")
    encrypted_key = json.loads(file.read())['os_crypt']['encrypted_key']
    print(f"encrypted_key: {encrypted_key}")
    print("*"*30)
encrypted_key = base64.b64decode(encrypted_key)                                       # Base64 decoding
encrypted_key = encrypted_key[5:]                                                     # Remove DPAPI
decrypted_key = win32crypt.CryptUnprotectData(encrypted_key, None, None, None, 0)[1]  # Decrypt key
print(f"decrypted_key: {decrypted_key}")

# cnx.execute("SELECT * FROM cookies")
# df = pd.read_sql_query("SELECT * FROM cookies ", cnx)
df = pd.read_sql_query("SELECT * FROM 'cookies' where host_key LIKE  '%insta%'", cnx)
# print(df["encrypted_value"])
plain_text_list, host_key_list, name_list = [], [], []
for index in range(len(df['host_key'])):
    data = df["encrypted_value"][index]
    host_key = df['host_key'][index]
    name = df['name'][index]
    nonce = data[3:3+12]
    ciphertext = data[3+12:-16]
    tag = data[-16:]
    # print(nonce,ciphertext,tag)
    cipher = AES.new(decrypted_key, AES.MODE_GCM, nonce=nonce)
    plaintext = cipher.decrypt_and_verify(ciphertext, tag) # the decrypted cookie
    plain_text_list.append(plaintext.decode("utf-8"))
    host_key_list.append(host_key.decode("utf-8"))
    name_list.append(name.decode("utf-8"))
    print(f"host_key: {host_key.decode('utf-8')}, plainText: {plaintext.decode('utf-8')}")
# cookies_datafram = {"Host_Key": host_key_list,
#                     "plain_text": plain_text_list}
# df.to_csv('decrypted_cookies.csv')
#**********************************************************
print("*"*60)
# print(f"host_key_list: {host_key_list}")
# print(f"plainText: {plain_text_list}")
# print(f"name: {name_list}")
cookie = {}
cookie = dict(zip(name_list, plain_text_list))
print(f"cookie: {cookie}")

