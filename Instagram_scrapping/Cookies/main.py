import os
import json
import base64 
import win32crypt
from Crypto.Cipher import AES

path = r'%LocalAppData%\Google\Chrome\User Data\Local State'
path = os.path.expandvars(path)
print(f"Path: {path}")
with open(path, 'r', encoding='ascii') as file:
    print("**********Opening file************")
    encrypted_key = json.loads(file.read())['os_crypt']['encrypted_key']
    print(f"encrypted_key: {encrypted_key}")
    print("*"*30)
encrypted_key = base64.b64decode(encrypted_key)                                       # Base64 decoding
encrypted_key = encrypted_key[5:]                                                     # Remove DPAPI
decrypted_key = win32crypt.CryptUnprotectData(encrypted_key, None, None, None, 0)[1]  # Decrypt key
print(f"decrypted_key: {decrypted_key}")


# data = bytes.fromhex('76313053514c69746520666f726d6174203300') # the encrypted cookie
# "v10�!J��A�"S	1���d��G��a����g,��OZ]j�8z���a{@�)"
data = bytes.fromhex('76313053514c69746520666f726d6174203300') # the encrypted cookie
nonce = data[3:3+12]
ciphertext = data[3+12:-16]
tag = data[-16:]
print(f"tag: {tag}")
cipher = AES.new(decrypted_key, AES.MODE_GCM, nonce=nonce)
plaintext = cipher.decrypt_and_verify(ciphertext, tag) # the decrypted cookie
print(plaintext)