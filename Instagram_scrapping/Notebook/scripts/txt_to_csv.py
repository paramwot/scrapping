import glob
import os
import json
import pandas as pd

os.chdir(r'Notebook/liker_username_data')
myFiles = glob.glob('*.txt"')

id_list = []
username_list = []
full_name_list = []
profile_oic_url_list = []
is_private_list = []
is_verified_list = []
followed_by_viewer_list = []

for file_name in myFiles:
    file = open(file_name, "r+", encoding="utf-8", )
    json_d = json.load(file)
    node = json_d['data']['shortcode_media']['edge_liked_by']['edges']
    total_node = len(json_d['data']['shortcode_media']['edge_liked_by']['edges'])
    for index in range(total_node):
        id_list.append(node[index]['node']['id'])
        username_list.append(node[index]['node']['username'])
        full_name_list.append(node[index]['node']['full_name'])
        profile_oic_url_list.append(node[index]['node']['profile_pic_url'])
        is_private_list.append(node[index]['node']['is_private'])
        is_verified_list.append(node[index]['node']['is_verified'])
        followed_by_viewer_list.append(node[index]['node']['followed_by_viewer'])
        print(f'{len(set(username_list))}_unique_usernames')
        myDataFrame = pd.DataFrame(
            {
                'id_list': id_list,
                'username_list': username_list,
                'full_name_list': full_name_list,
                'profile_oic_url_list': profile_oic_url_list,
                'is_private_list': is_private_list,
                'is_verified_list': is_verified_list,
                'followed_by_viewer_list': followed_by_viewer_list
            }
        )
        myDataFrame.to_csv('18_05_1011Liker_data.csv')