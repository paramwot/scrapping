# Imporing required libraries
import requests
from datetime import datetime
import re
import io
import PIL.Image as Image
import pandas as pd

# Function to convert cookies as a str to dict
def cookie_to_dict(cookie):
    """
    takes string and returns dictionary for cookies
    params: cookie (string)
    return cookie (dictionary)
    """
    cookie_list = cookie.split(";")
    dictionary = {}
    for cookie_values in cookie_list:
        dict_split = cookie_values.split("=")
        dictionary[dict_split[0]] = ''.join(dict_split[1:])
    print(dictionary)
    return dictionary


# Headers and cookie (change according to post)
headers = {
    'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Mobile Safari/537.36'
}
cookie = 'mid=YMm5sQAEAAHpgfySPNTt2AlEGZuF; ig_did=6DC68DD4-FF22-4BDA-A423-33DDCAE874B6; ig_nrcb=1; csrftoken=varRpkeiKTzTUuH1OWEKQEKJuMeIyZTp; ds_user_id=48136089851; sessionid=48136089851%3AkjmNggeiUz0gRg%3A21; rur=FRC'

# Request URL
request_url = 'https://www.instagram.com/adidasoriginals/'
request_url_list = re.split("/", request_url)
username = request_url_list[-2]
link = f'https://www.instagram.com/{username}/?__a=1' #request url


# Making get request
page = requests.get(link, cookies=cookie_to_dict(cookie), headers=headers)
print(f'page response:{page}')

# pre-processing for seo category
n = len(page.json()['seo_category_infos'])
seo_category_value_list_processed = []
seo_category_value_list = [page.json()['seo_category_infos'][i] for i in range(n)]
for j in range(len(seo_category_value_list)):
    n = len(seo_category_value_list[j])
    for k in range(n):
        seo_category_value_list_processed.append(seo_category_value_list[j][k])
        break

data_dict = {
    "date_of_scrapping": datetime.today().strftime('%Y-%m-%d'),
    "account_URL": request_url,
    'account_photo': page.json()['graphql']['user']['profile_pic_url_hd'],
    "account_name" : page.json()['graphql']['user']['full_name'],
    'number_of_posts': None,
    'number_of_followers': page.json()['graphql']['user']['edge_followed_by']['count'],
    'number_of_following': page.json()['graphql']['user']['edge_follow']['count'],
    'account_description': page.json()['graphql']['user']['biography'],
    'account_description_URL': page.json()['graphql']['user']['external_url'],
    'business_category_name': page.json()['graphql']['user']['business_category_name'],
    'business_email': page.json()['graphql']['user']['business_email'],
    'business_phone_number': page.json()['graphql']['user']['business_phone_number'],
    'category_name': page.json()['graphql']['user']['category_name'],
    'business_address': page.json()['graphql']['user']['business_address_json'],
    'has_ar_effects': page.json()['graphql']['user']['has_ar_effects'],
    'highlight_reel_count': page.json()['graphql']['user']['highlight_reel_count'],
    'is_business_account': page.json()['graphql']['user']['is_business_account'],
    'is_professional_account': page.json()['graphql']['user']['is_professional_account'],
    'is_verified': page.json()['graphql']['user']['is_verified'],
    'seo_category': seo_category_value_list_processed
}
# To save profile_picture
with open('account_photo', 'wb') as handle:
    response = requests.get(data_dict['account_photo'], stream=True)

    if not response.ok:
        print(response)

    for block in response.iter_content(1024):
        if not block:
            break
        handle.write(block)

df = pd.DataFrame([data_dict])
df.to_csv('profile_data.csv')