# Imporing required libraries
import requests
from datetime import datetime
import re
import io
import PIL.Image as Image
import os
import time
import pandas as pd
import random

# Function to convert cookies as a str to dict
def cookie_to_dict(cookie):
    """
    takes string and returns dictionary for cookies
    params: cookie (string)
    return cookie (dictionary)
    """
    cookie_list=cookie.split(";")
    dictionary={}
    for cookie_values in cookie_list:
        dict_split=cookie_values.split("=")
        dictionary[dict_split[0]]=''.join(dict_split[1:])
    print(dictionary)
    return dictionary


# Headers and cookie (change according to post)
headers = {

    'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.101 Mobile Safari/537.36'
}
cookie = 'mid=YMm5sQAEAAHpgfySPNTt2AlEGZuF; ig_did=6DC68DD4-FF22-4BDA-A423-33DDCAE874B6; ig_nrcb=1; csrftoken=varRpkeiKTzTUuH1OWEKQEKJuMeIyZTp; ds_user_id=48136089851; sessionid=48136089851%3AkjmNggeiUz0gRg%3A21; rur=FRC'

request_url = 'https://www.instagram.com/graphql/query/?query_hash=7ea6ae3cf6fb05e73fcbe1732b1d2a42&variables={%22id%22:%229187952%22,%22first%22:12,%22after%22:%22QVFBOV96T3puVnJ5bUg1QW9FelZDdDlNT0U1ekRBOXFBcmN1a0VNM2RRb2dLZnFMR3hwZGNZcmxDTjBlN2dQUDhtTHI4Qkk5ckZPbUxic09KUEpEekw3Xw==%22}'

# First response, passing params as variable dict
first_page = requests.get(request_url, cookies = cookie_to_dict(cookie), headers = headers)
print(first_page)

has_next_page = first_page.json()['data']['user']['edge_owner_to_timeline_media']['page_info']['has_next_page']
end_cursor = first_page.json()['data']['user']['edge_owner_to_timeline_media']['page_info']['end_cursor']

post_count = 0
display_url_list = []
is_video_list = []
has_upcoming_event_list = []
caption_list = []
comments_count_list = []
is_paid_partnership_list = []
comments_disabled_list = []
likes_list = []
location_list = []
thumbnail_src_list = []

def save_image(name, image_url):
    with open(name, 'wb') as handle:
                response = requests.get(image_url, stream=True)
                if not response.ok:
                    print(response)
                for block in response.iter_content(1024):
                    if not block:
                        break
                    handle.write(block)


total_edges = len(first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'])
dir_path = f'post_images/'
for index in range(total_edges):
    post_count += 1
    print(f'post_count: {post_count}')
    display_url = first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['display_url']
    is_video = first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['is_video']
    has_upcoming_event = first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['has_upcoming_event']
    caption = first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['edge_media_to_caption']['edges'][-1]['node']['text']
    comments_count = first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['edge_media_to_comment']['count']
    is_paid_partnership = first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['is_paid_partnership']
    comments_disabled = first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['comments_disabled']
    likes_count = first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['edge_media_preview_like']['count']
    location = first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['location']
    thumbnail_src = first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['thumbnail_src']
    os.mkdir(f'{dir_path}post_{post_count}')
    save_path = f'{dir_path}post_{post_count}'
    print(save_path)
    try:
        total_images_in_post = len(first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['edge_sidecar_to_children']['edges'])
        for image_url in range(int(total_images_in_post)):
            img_url = first_page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['edge_sidecar_to_children']['edges'][image_url]['node']['display_url']
            save_image(save_path+'/'+str(post_count)+'_post_'+str(image_url)+'_photo', img_url)
    except:
        save_image(save_path+'/'+'post_'+str(post_count), display_url)
    display_url_list.append(display_url)
    is_video_list.append(is_video)
    has_upcoming_event_list.append(has_upcoming_event)
    caption_list.append(caption)
    comments_count_list.append(comments_count)
    is_paid_partnership_list.append(is_paid_partnership)
    comments_disabled_list.append(comments_disabled)
    likes_list.append(likes_count)
    location_list.append(location)
    thumbnail_src_list.append(thumbnail_src)
    f=open(f'post_response/{post_count}.txt"',"w+", encoding="utf-8")
    f.write(first_page.text)
    f.close()


updated_url = 'https://www.instagram.com/graphql/query/?query_hash=7ea6ae3cf6fb05e73fcbe1732b1d2a42'

variables = {"id":"9187952","first":12,"after":"QVFDbkF2WnhselEzVllPVkZMdEdvNnZFMGJhcXV4amhqOFNWT01HMF9VNHZ0b29iQS1pM3Vic2NBc3BqMXVsYzREeUNpbUlwb0g2ci1hMXdhUXoxT01EYQ=="}

has_next_page = True
end_cursor = first_page.json()['data']['user']['edge_owner_to_timeline_media']['page_info']['end_cursor']
# variables['after'] = 'QVFBeTczdXQySnpueVNiX0ZYcXlpdlltLWMwNElsNWQ0WGRIUHFkTXJNUlpCVzk5QmhtLUc3a042VjlLQ3ZreHdjeHlqQTdlMjRURTN6U2ZrWDN3ajV0Yw=='
variables['after'] = end_cursor
page_request = 346

while has_next_page == True:
    time.sleep(random.randint(3, 5))
    page_request += 1
    print("----------------------------------------------------")
    print(f'post count: {page_request}')
    page = requests.get(updated_url, headers=headers, cookies=cookie_to_dict(cookie), params=variables)
    print("----------------------------------------------------")
    print(page.url)
    total_edges = len(page.json()['data']['user']['edge_owner_to_timeline_media']['edges'])
    dir_path = f'post_images/'
    for index in range(total_edges):
        post_count += 1
        print("----------------------------------------------------")
        print(f'post_count: {post_count}')
        display_url = page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['display_url']
        is_video = page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['is_video']
        has_upcoming_event = page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node'][
            'has_upcoming_event']
        caption = \
        page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['edge_media_to_caption'][
            'edges'][-1]['node']['text']
        comments_count = \
        page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['edge_media_to_comment'][
            'count']
        is_paid_partnership = page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node'][
            'is_paid_partnership']
        comments_disabled = page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node'][
            'comments_disabled']
        likes_count = \
        page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['edge_media_preview_like'][
            'count']
        location = page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node']['location']
        thumbnail_src = page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node'][
            'thumbnail_src']
        os.mkdir(f'{dir_path}post_{post_count}')
        save_path = f'{dir_path}post_{post_count}'
        print(save_path)
        try:
            total_images_in_post = len(
                page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node'][
                    'edge_sidecar_to_children']['edges'])
            for image_url in range(int(total_images_in_post)):
                img_url = page.json()['data']['user']['edge_owner_to_timeline_media']['edges'][index]['node'][
                    'edge_sidecar_to_children']['edges'][image_url]['node']['display_url']
                save_image(save_path + '/' + str(post_count) + '_post_' + str(image_url) + '_photo', img_url)
        except:
            save_image(save_path + '/' + 'post_' + str(post_count), display_url)
        display_url_list.append(display_url)
        is_video_list.append(is_video)
        has_upcoming_event_list.append(has_upcoming_event)
        caption_list.append(caption)
        comments_count_list.append(comments_count)
        is_paid_partnership_list.append(is_paid_partnership)
        comments_disabled_list.append(comments_disabled)
        likes_list.append(likes_count)
        location_list.append(location)
        thumbnail_src_list.append(thumbnail_src)
        f = open(f'post_response/{post_count}.txt"', "w+", encoding="utf-8")
        f.write(page.text)
        f.close()

    has_next_page = page.json()['data']['user']['edge_owner_to_timeline_media']['page_info']['has_next_page']
    if has_next_page == True:
        end_cursor = page.json()['data']['user']['edge_owner_to_timeline_media']['page_info']['end_cursor']
        variables['after'] = end_cursor


post_data_datafram = pd.DataFrame(
    {'display_url': display_url_list,
     'is_video': is_video_list,
     'has_upcoming_event': has_upcoming_event_list,
     'caption': caption_list,
     'comments_count': comments_count_list,
     'is_paid_partnership': is_paid_partnership_list,
     'comments_disabled': comments_disabled_list,
     'likes': likes_list,
     'location': location_list,
     'thumbnail_src':thumbnail_src_list
    })

post_data_datafram.to_csv('post_csv/post_data_2.csv')