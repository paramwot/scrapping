# Request Module 
***
#### Following repository contains 3 python files
* ```1. request_basics.py``` for basics of request library (Module)
* ```2. nasa_api.py``` for downloading image using nasa API
* ```3. parsing.py``` for parsing in different modes
* ```4. cookie.py``` for login using cookie
***
> Note ~ Request module is for getting information from website. But it's not ment for parsing the information.
To parse information from website, need to use Beautifulsoup or Request-HTML. 
#### GET request
* get or retrieve data from a specified resource
* requests.get()
* status code: Provide information about request status
```bigquery
response = requests.get('https://api.github.com')
print(response.status_code)
> 404
```
* __200__: Everything went okay, and the result has been returned (if any).
* __301__: The server is redirecting you to a different endpoint. This can happen when a company switches domain names, or an endpoint name is changed.
* __400__: The server thinks you made a bad request. This can happen when you don’t send along the right data, among other things.
* __401__: The server thinks you’re not authenticated. Many APIs require login ccredentials, so this happens when you don’t send the right credentials to access an API.
* __403__: The resource you’re trying to access is forbidden: you don’t have the right permissions to see it.
* __404__: The resource you tried to access wasn’t found on the server.
* __503__: The server is not ready to handle the request.
***
* To see the response’s content in bytes
```
response = requests.get('https://api.github.com')
response.content
```
* To see response in string
```bigquery
response.text
'{"current_user_url":"https://api.github.com/user","current_user_authorizations_html_url":"https://github.com/settings/connections/applications{/client_id}","authorizations_url":"https://api.github.com/authorizations","code_search_url":"https://api.github.com/search/code?q={query}{&page,per_page,sort,order}","commit_search_url":"https://api.github.com/search/commits?q={query}{&page,per_page,sort,order}","emails_url":"https://api.github.com/user/emails","emojis_url":"https://api.github.com/emojis","events_url":"https://api.github.com/events","feeds_url":"https://api.github.com/feeds","followers_url":"https://api.github.com/user/followers","following_url":"https://api.github.com/user/following{/target}","gists_url":"https://api.github.com/gists{/gist_id}","hub_url":"https://api.github.com/hub","issue_search_url":"https://api.github.com/search/issues?q={query}{&page,per_page,sort,order}","issues_url":"https://api.github.com/issues","keys_url":"https://api.github.com/user/keys","notifications_url":"https://api.github.com/notifications","organization_repositories_url":"https://api.github.com/orgs/{org}/repos{?type,page,per_page,sort}","organization_url":"https://api.github.com/orgs/{org}","public_gists_url":"https://api.github.com/gists/public","rate_limit_url":"https://api.github.com/rate_limit","repository_url":"https://api.github.com/repos/{owner}/{repo}","repository_search_url":"https://api.github.com/search/repositories?q={query}{&page,per_page,sort,order}","current_user_repositories_url":"https://api.github.com/user/repos{?type,page,per_page,sort}","starred_url":"https://api.github.com/user/starred{/owner}{/repo}","starred_gists_url":"https://api.github.com/gists/starred","team_url":"https://api.github.com/teams","user_url":"https://api.github.com/users/{user}","user_organizations_url":"https://api.github.com/user/orgs","user_repositories_url":"https://api.github.com/users/{user}/repos{?type,page,per_page,sort}","user_search_url":"https://api.github.com/search/users?q={query}{&page,per_page,sort,order}"}'
```
* To get dictionary 
```bigquery
response.json()
```
****
### Headers
* some useful information, such as the content type of the response payload and a time limit on how long to cache the response.
```bigquery
response.headers
{'Server': 'GitHub.com', 'Date': 'Thu, 03 Jun 2021 12:29:47 GMT', 'Cache-Control': 'public, max-age=60, s-maxage=60', 'Vary': 'Accept, Accept-Encoding, Accept, X-Requested-With', 'ETag': '"27278c3efffccc4a7be1bf315653b901b14f2989b2c2600d7cc2e90a97ffbf60"', 'Access-Control-Expose-Headers': 'ETag, Link, Location, Retry-After, X-GitHub-OTP, X-RateLimit-Limit, X-RateLimit-Remaining, X-RateLimit-Used, X-RateLimit-Resource, X-RateLimit-Reset, X-OAuth-Scopes, X-Accepted-OAuth-Scopes, X-Poll-Interval, X-GitHub-Media-Type, Deprecation, Sunset', 'Access-Control-Allow-Origin': '*', 'Strict-Transport-Security': 'max-age=31536000; includeSubdomains; preload', 'X-Frame-Options': 'deny', 'X-Content-Type-Options': 'nosniff', 'X-XSS-Protection': '0', 'Referrer-Policy': 'origin-when-cross-origin, strict-origin-when-cross-origin', 'Content-Security-Policy': "default-src 'none'", 'Content-Type': 'application/json; charset=utf-8', 'X-GitHub-Media-Type': 'github.v3; format=json', 'Content-Encoding': 'gzip', 'X-RateLimit-Limit': '60', 'X-RateLimit-Remaining': '55', 'X-RateLimit-Reset': '1622726729', 'X-RateLimit-Resource': 'core', 'X-RateLimit-Used': '5', 'Accept-Ranges': 'bytes', 'Content-Length': '512', 'X-GitHub-Request-Id': 'C184:4475:28B816:2D5686:60B8CB46'}
```
```bigquery
response.headers['Content-Type']
'application/json; charset=utf-8'
```

***
### How OAuth workes?
>1.The Spotify app will ask the Facebook API to start an authentication flow. To do this, the Spotify app will send its application ID (client_id) and a URL (redirect_uri) to redirect the user after success or error.
>> 2.You’ll be redirected to the Facebook website and asked to log in with your credentials. The Spotify app won’t see or have access to these credentials. This is the most important benefit of OAuth.
>>> 3.Facebook will show you all the data the Spotify app is requesting from your profile and ask you to accept or reject sharing that data.
>>>> 4.If you accept giving Spotify access to your data, then you’ll be redirected back to the Spotify app, already logged in.
***
### Rate Limiting
* Given that APIs are public facing and can be used by anyone, people with bad intentions often try to abuse them. To prevent such attacks, you can use a technique called rate limiting, which restricts the number of requests that users can make in a given time frame.

# Cookie
- HTTP cookies help web developers give you more personal, convenient website visits. Cookies let websites remember you, your website logins, shopping carts and more. 
- Cookies are text files with small pieces of data — like a username and password — that are used to identify your computer as you use a computer network. Specific cookies known as HTTP cookies are used to identify specific users and improve your web browsing experience.
- Data stored in a cookie is created by the server upon your connection. This data is labeled with an ID unique to you and your computer.
- types: 
  * Magic Cookie
  * HTTP Cookie
    
> A ___cookie___ is basically just an item in a dictionary. Each item has a key and a value. For authentication, the key could be something like 'username' and the value would be the username. Each time you make a request to a website, your browser will include the cookies in the request, and the host server will check the cookies. So authentication can be done automatically like that.
![cookie/session example](/home/param/BitBuckte Repos/scrapping/Learning/images/cookie.png)