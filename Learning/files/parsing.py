import requests
from requests.exceptions import HTTPError
from bs4 import BeautifulSoup

for endpoint in ['https://en.wikipedia.org/wiki/Web_scraping']:
    try:
        response = requests.get(endpoint)

        # If the response made successful, no exception will be raise
        response.raise_for_status()

        # Parsing into HTMl
        # BS4_object = BeautifulSoup(response.text, 'html.parser')

        # PArsing into XML (DOM)
        # BS4_object = BeautifulSoup(response.text, 'lxml')
        # print(BS4_object.h2)

    except HTTPError as http_erro:
        print(f'HTTP error occured: {http_erro}')
    except Exception as error:
        print(f'Other error occured: {error}')
    else:
        print('Sucess!')