import requests
from bs4 import BeautifulSoup

header = {
   'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Mobile Safari/537.36'
}
login_data = {
    'name': 'param_t09',
    'pass': 'param@xxxx',
    'form_id': 'new_login_form',
    'op': 'Login'
}

def get_cookie():
    session = requests.Session()
    session.get('https://www.codechef.com/')
    session_cookie = session.cookies
    cookie_dict = session_cookie.get_dict()
    print(cookie_dict)

def login_to_codechef():
    with requests.Session() as session_obj:
        url = 'https://www.codechef.com/'
        response = session_obj.get(url, headers=header)
        # print(response.content)
        soup_obj = BeautifulSoup(response.content, 'html.parser')
        login_data['form_build_id'] = soup_obj.find('input', attrs={'name': 'form_build_id'})['value']
        response = soup_obj.post(url, data= login_data, headers = header)
        print(response.content)