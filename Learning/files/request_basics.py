import requests
from requests.exceptions import HTTPError

# url = 'https://xkcd.com/353/', 'https://httpbin.org'

for url in ['https://httpbin.org']:
    try:
        response = requests.get(url)

        # If the response made successful, no exception will be raise
        response.raise_for_status()

        # To get response payload
        # print(response.text)

        # To get response dict
        # print(response.json())

        # To get headers
        # print(response.headers)

        # To save image
        # response = requests.get('https://imgs.xkcd.com/comics/python.png')
        # # print(response.content) # Gives bites from image
        # with open('comic.png', 'wb') as file:
        #     file.write(response.content)

        # status code
        # print(response.status_code)

        # GET method
        # payload = {'page': 2, 'count': 25}
        # response = requests.get('https://httpbin.org/get', params=payload)
        # print(response.text)
        # print(response.json()) # json data
        # print(response.url) # exect url

        # POST method
        # payload = {'username': 'param', 'password': "testing"}
        # response = requests.post('https://httpbin.org/post', data=payload_post)
        # print(response.text)

        # Authentication
        # response = requests.get('https://httpbin.org/basic-auth/param/testing', auth=('param', 'testing'))
        # print(response.text)

        # GET method example
        # response = requests.get(
        #     'https://api.github.com/search/repositories',
        #     params= {'q': 'request_language:python'},
        # )
        # json_response = response.json()
        # repository = json_response['items'][0]
        # print(f'Repository name: {repository["name"]}')
        # print(f'Repository description: {repository["description"]}')

        # Authentication in Github
        # response = requests.get(
        #     'https://api.github.com/user',
        #     auth = ('parammteraiya09', 'paramxx@x')
        # )
        # print(response.text)

    except HTTPError as http_err:
        print(f'HTTP error occured: {http_err}')
    except Exception as err:
        print(f'Other error occured: {err}')
    else:
        print('Sucess!')