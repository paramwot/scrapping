import requests
from requests.exceptions import HTTPError

for endpoint in ['https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos']:
    try:
        api_key = "Bg3bnDH7ejanktt06DSh5LQjTEaR7jN1DUrfiKqk"
        query_params = {"api_key": api_key, "earth_date": "2021-06-03"}
        response = requests.get(endpoint, params= query_params)

        # If the response made successful, no exception will be raise
        response.raise_for_status()

        # print(response.json())
        photos = response.json()["photos"]
        print(f'Found {len(photos)} photos')
        photo_url = photos[4]['img_src']
        photo_response = requests.get(photo_url)
        with open('marse.png', 'wb') as file:
            file.write(photo_response.content)



    except HTTPError as http_erro:
        print(f'HTTP error occured: {http_erro}')
    except Exception as error:
        print(f'Other error occured: {error}')
    else:
        print('Sucess!')